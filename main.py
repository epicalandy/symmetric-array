def main():
    array_size = get_array_size()
    user_array = get_user_array(array_size)
    result = is_symmetric_array(user_array)

    print('Размер массива: ' + str(array_size))
    print('Пользовательский массив: ' + str(user_array))
    print('Результат проверки на симметричность: ' + result)

    return 'success'


def get_array_size():
    """ Получить от пользователя размер массива """

    size = input('Введите размер массива (от 1 до 100): ')

    if not size.isdigit():
        print('Необходимо ввсети число')

        return get_array_size()

    result = int(size)

    if 0 >= result > 100:
        print('Число должно быть в диапазоне 1 - 100')

        return get_array_size()

    return result


def get_user_array(array_size):
    """ Получить от пользователя значения чисел массива построчно """

    print('Опишите массив')
    user_array = []
    iter = 0

    while iter != array_size:
        iter += 1
        temp = get_next_string(iter, array_size)
        user_array.append(temp)

    return user_array


def get_next_string(sting_number, count):
    """ Получить от пользователя строку ччисел массива """

    numbers = input('Введите ' + str(sting_number) + ' строку: ')
    temp = numbers.split(' ')
    if not is_numbers_string(temp, count):
        print('Не корректно введена строка. Формат "X X X", количество чисел ' + str(count))

        return get_next_string(sting_number, count)

    return temp


def is_numbers_string(user_string, count):
    """ Проверить введённую пользователем строку на соблюдения условий описания массива """

    if len(user_string) != count:
        print('debugger[1]: ' + str(user_string))

        return False

    for element in user_string:
        if not element.isdigit():
            print('debugger[2]: ' + str(element))

            return False

    return True


def is_symmetric_array(arr):
    """ Проверить массив на симметричность """

    array_size = len(arr)
    row = 0
    col = 1

    while row != array_size:
        row += 1

        while col:
            col = row - 1

            if arr[row][col] != arr[col][row]:
                return 'no'

    return 'yes'


main()
